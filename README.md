# Sons of the King Website

## How to Run
Navigate to the project root in a terminal and run `rails s`. Then navigate to `localhost:3000` in a web browser.


### Blog ideas:
Let users style text with [Editor.md](https://pandao.github.io/editor.md/) and save the markdown in the db. When we pull it out, render it to HTML using the `redcarpet` gem (see [this](https://richonrails.com/articles/rendering-markdown-with-redcarpet) tutorial).

### Front-end
[Bootstrap](https://getbootstrap.com/docs/4.1/components/alerts/)

### Content Management
Use [X-Editable](https://vitalets.github.io/x-editable/)

### To Do
- create landing page
- add static pages
- implement admin authentication system
- implement bootstrap 4 [instructions](https://github.com/twbs/bootstrap-rubygem)
- create blog
  - implement markdown
  - implement image-uploading

